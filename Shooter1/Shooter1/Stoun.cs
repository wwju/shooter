﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class Stoun:Position //Этот класс будет 
        //служить для наличия (они есть, но невидимы)
        //на карте непроходимых мест
    {
        //после создания класс не работал, о чем я написал еще в классе 
        //Map, после некоторых доработок он создался. Нужно было дописать
        //в Way.IsCanGet сравнение с 
        public Rectangle region;
        protected Texture2D t;
        String coor; //переменную ввел для проверки того, 
        //какие параметры имеет создаваемая непроходимая область
        SpriteFont spriteFont;
        int number;

        public Stoun(Rectangle region, ContentManager Manager, int n)
            : base(new Vector2(region.X,region.Y))
        {
            number = n;
            this.region = region;
            AllStouns.AddTexture(this);
            t = Manager.Load<Texture2D>("Grass");
            spriteFont = Manager.Load<SpriteFont>("MyFont");
            coor = "x:" + Convert.ToString(region.X) + " y:" + 
                Convert.ToString(region.Y) + " w:" + 
                Convert.ToString(region.Width) + " h:" +
                Convert.ToString(region.Height) + " n:" +
                Convert.ToString(number);
        }

        public virtual void Update()
        {
            //Обнаружилось странное явление: при установлении with и height 
            //rectangle они почему-то стали равняться единице. Попробую ис-
            //пользовать стандартную функцию сравнения прямоугольников,
            //а не собственнонаписанную. Для меня стало удивлением то, что
            //со стандартной. Все отлично, значит, можно пытаться делать
            //чтение из файла для составления карт. Запись в файл также может оч
            //пригодиться
            region = new Rectangle((int)position.X, (int)position.Y, region.Width, region.Height);
            coor = "x:" + Convert.ToString(region.X) + " \ny:" +
                Convert.ToString(region.Y) + " \nw:" +
                Convert.ToString(region.Width) + " \nh:" +
                Convert.ToString(region.Height) + " \nn:" +
                Convert.ToString(number);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(t, position, Color.White);
            spriteBatch.DrawString(spriteFont, coor, position, Color.Black);
        }
    }
}
