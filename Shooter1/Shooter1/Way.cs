﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    //при обращении к статической функции
    //некст возвращается значение в виде 
    //вектора, ближайшее к точке У. Путь
    //начинается из точки Х
    public static class Way //Статический класс, при обращении к функциям которого
        //можно найти следующие точки, ближайшие точки движения, расстояния
    {
        
        static public double Distance(Vector2 x, Vector2 y) //Статическая функция найдет
            //расстояние между двумя точками
            //а еще у нее нужно сменить имя - я сменил имя с put
        {
            return Math.Sqrt(Math.Pow(y.Y - x.Y, 2) + Math.Pow(y.X - x.X, 2));
        }

        
        //ну все, наконец-то эта процедура по определению следующей точки в движении
        //текстуры доделана (почти) - работает, но нужно поработать над производительностью
        //над производительностью поработал, теперь функция определения угла вынесена в 
        //статический класс Angle, из которого я могу получать угол в градусах, а не только
        //в радианах
        public static Vector2 NextPoint(Vector2 a, // точка текстуры
            double alpha // угол альфа
            ) // будущая х-координата
        {
            //здесь необходимо переписать - использовать правильные
            //ифы, таким образом, чтобы 


            //а - координата текстуры

            //альфа - текущий угол наклона текстуры,
            //также он равен углу между курсором

            //х - новая координата иксов
            //у - новая координата игрек
            float y, x;
            if (a.X == Cursor.position.X) //это вроде верно, если точки находятся на одной вертикальной оси
                //линии, то:
            {
                if (a.Y == Cursor.position.Y)
                    return new Vector2(0, 0); //возвратить нули (текстура не будет двигаться)
                else
                    if (a.Y > Cursor.position.Y) 
                        return new Vector2(a.X, a.Y - 1); // если текстура ниже курсора (ее координата
                        //больше), то иксы оставить на месте, но игреки декрементировать
                    else
                        return new Vector2(a.X, a.Y + 1); // если текстура выше курсора (ее координата
                //меньше), то иксы оставить на месте, но игреки инкрементировать
            }
            else //здесь сейчас нужно исправить, когда найду бумажку с 
                //формулами, хотя, я попытаюсь исправить это и так
                if (a.X > Cursor.position.X) //если текстура правее курсора
                {
                      // новая координата по иксу
                    if (a.Y == Cursor.position.Y) //если текстура правее, а с курсором на одной горизонтальной оси
                    {
                        return new Vector2(a.X - 1, a.Y);     //икс изменится, игрек - нет
                    }
                    else
                    {
                        if (a.Y > Cursor.position.Y)
                        {
                            x = (float)(1 * Math.Cos(alpha));
                            y = (float)(1 * Math.Sin(alpha));
                            return new Vector2(a.X + x, a.Y + y);
                        }
                        else
                        {
                            x = (float)(1 * Math.Cos(alpha));
                            y = (float)(1 * Math.Sin(alpha));
                            return new Vector2(a.X - x, a.Y - y);
                        }
                    }
                }
                else
                {
                    if (a.Y == Cursor.position.Y) //если текстура правее, а с курсором на одной горизонтальной оси
                    {
                        return new Vector2(a.X + 1, a.Y);     //икс изменится, игрек - нет
                    }
                    else
                    {
                        if (a.Y > Cursor.position.Y)
                        {
                            x = (float)(1 * Math.Cos(alpha));
                            y = (float)(1 * Math.Sin(alpha));
                            return new Vector2(a.X + x, /*a.Y +*/ a.Y + y);
                        }
                        else
                        {
                            x = (float)(1 * Math.Cos(alpha));
                            y = (float)(1 * Math.Sin(alpha));
                            return new Vector2(a.X + x, a.Y + y);
                        }
                    }
                }
        }


        public static Vector2 Speed(Vector2 a, double alpha, float speed = 1) //Статическая функция возвращает
            //скорость, с которой должна двигаться точка а, в точку курсора и далее. Комментарии
            //оставлю из процедуры НэксТан (ее я переименую чуть позже в более адекватную), 
            //хотя, нет, комментарии уберу. Возможно также в качестве третьего параметра добавить скорость
        {
            float y, x;
            if (a.X == Cursor.position.X) 
            {
                if (a.Y == Cursor.position.Y)
                    return new Vector2(0, 0); 
                else
                    if (a.Y > Cursor.position.Y) 
                        return new Vector2(0, -speed); 
                    else
                        return new Vector2(0, speed); 
            }
            else
                if (a.X > Cursor.position.X)
                {
                    if (a.Y == Cursor.position.Y)
                    {

                        return new Vector2(-speed, 0);
                    }
                    else
                    {
                        if (a.Y > Cursor.position.Y)
                        {
                            x = (float)(speed * Math.Cos(alpha));
                            y = (float)(speed * Math.Sin(alpha));
                            return new Vector2(x, y);
                        }
                        else
                        {
                            x = (float)(speed * Math.Cos(alpha));
                            y = (float)(speed * Math.Sin(alpha));
                            return new Vector2(-x, -y);
                        }
                    }
                }
                else
                {
                    if (a.Y == Cursor.position.Y)
                    {
                        return new Vector2(speed, 0);
                    }
                    else
                    {
                        if (a.Y > Cursor.position.Y)
                        {
                            x = (float)(speed * Math.Cos(alpha));
                            y = (float)(speed * Math.Sin(alpha));
                            return new Vector2(x, y);
                        }
                        else
                        {
                            x = (float)(speed * Math.Cos(alpha));
                            y = (float)(speed * Math.Sin(alpha));
                            return new Vector2(x, y);
                        }
                    }
                }
        }

        public static Vector2 Speed2(Vector2 A, Vector2 M, float speed = 1) 
        {
            //Ура! Вот это и есть правильная функция!!! АААА 
            //Это очень круто! Яхуууууу Я ЭТО СДЕЕЕЭЭЛАЛ
            //Функцию выше оставлю на всякий случай - может, пригодится :)

            //а эту функцию нужно будет, конечно же, упростить. Но этим я
            //займусь позже. 
            double alpha;
            float By, Bx;
            alpha = Angle.GetAngleInRad(A, M) - 1.570796326794897; //функция
            //возвращала вектор, отклоненный на 90 градусов по часовой стрелки
            //в сторону. С чем это связано не знаю, и знать не особо хочу.
            //Надеюсь, в дальнейшем это не станет ошибкой-на-ошибке
            //Ах, да число 1.570796326794897 - это 90 градусов в радианах
            By = speed * (float)Math.Sin(alpha);
            Bx = speed * (float)Math.Cos(alpha);
            return new Vector2(Bx, By);
        }

        //я тут подумал, и решил функцию проверки на столкновения включить
        //в этот класс. Возможно его скоро придется переименовать в что-то 
        //наподобие GameMath или еще что-то более богатое фантазией, например
        //IgraMatematika :) Ну или эту функцию нужно будет перенести в другой 
        //класс, в котором будет вестись учет текстур, у которых происходит 
        //проверка столкновения
        //смысл в этой функции следующий: в качестве фактических параметров
        //ей передаются значения Rectangle текстуры (в данном случае героя)
        //и по-видимому, массив Rectangle[] (ах, да, объект статический, поэтому
        //функция сама будет обращаться к массиву), который будет доставаться из 
        //статического объекта с названием например, AllStouns - его нужно
        //будет назвать иначе, но пока у меня нет словаря, это единственное,
        //что я смог придумать. Ну или в героя, но я думаю, что эта функция
        //будет использоваться не только здесь
        //а вообще сейчас лучше просто будет организовать добавление одного 
        //Rectangle на карту и проверить, работает, или нет.
        //на основе данных Rectangle из героя строится новый Rectangle с уже измененными
        //координатами и далее проверяется, пересекаются ли эти прямоугольники. Пересеклись
        // - движение невозможно и функция выдаст false. В классе Hero нужно будет всего 
        //лишь организовать условие
        //и нужно очень подумать перед тем, как подобное делать полностью - 
        //есть два варианта:
        //1) карты будут рисоваться вручную, класс AllStouns при создании будет считывать
        //из текстового файла непроходимые области, и тем самым будет заполняться массив
        //типа Rectangle, который и будет передаваться функции IsCanGet. Смысл в том,
        //что все карты будут нарисованы вручную, и затем в специально созданном редакторе
        //я должен буду лично расставить непроходимые области (несложное занятие, просто квадратики
        //разной величины протянуть там где нужно
        //2) карты будут случайно генерироваться (для этого нужны мозги) или считываться из файла
        //,который я лично приготовлю в созданном мною редакторе или вручную (по-идее, если там
        //всего
        //два типа местности, то все будет отлично, в противном случае все печально, и чем
        //дальше, тем хуже). Карты не нужно будет прорисовывать, но они будут уродливыми квадрати
        //ками. А особенно удручает то, что на компе я даже эти квадратики не нарисую
        // :( наверное, хотя... иногда получалось. Ну и при создании карты все эти квадратики 
        //просто
        //запишутся как надо, и все будет ОК.
        //Нет, сперва нужно проверить действенность метода на одной текстуре. Она будет добавляться
        //прямо в функции IsCanGet - сделал - я просто вручную вписал координаты прямоугольника
        //в классе Hero 
        //if (Way.IsCanGet(region, new Vector2(speed, 0), new Rectangle(300, 300, 60, 60)))
        //при изменении этих координат на new Rectangle(100, 100, 60, 60), да и любые другие,
        //не соприкасающиеся с героем координаты, герой мог двигаться. В противном случае - нет
        //А сейчас нужно будет дорабатывать принципы наследования. Дело в том, что 
        //класс StandTexture предполагает то, что он не будет обрабатываться классом
        //AllTexture, а ведь для очень многих элементов будет важно наличие у них Rectangle,
        //даже несмотря на то что что в качестве карты я буду использовать преимущественно
        //первый вариант (в том смысле, что скорее всего). Очень удобно будет обрабатывать
        //попадания пуль, например.
        

        public static bool IsCanGet(Rectangle GetRectangle, Vector2 speed)
            //нужно подумать, что будет возвращать true. По-идее нужно будет возвратить
            //true в случае, если движение возможно. Так и сделаю
        {
            int i;
            Rectangle gr = new Rectangle((int)(GetRectangle.X - speed.X), 
                (int)(GetRectangle.Y - speed.Y), 
                GetRectangle.Width, 
                GetRectangle.Height); //Вот же я дибилище лесное! Сижу, ржу и чуть не плачу (последнее неправда)
            //Настолько тупая ошибка. Что я только не делал! Оказалось все просто - вместо 
            //плюсов нужно поставить минусы, поскольку в классе MoveTextures происходит инкремент всех текстур
            //а я, наивный, передвигаю region в сторону, обратную стороне движения героя, проверяю его на 
            //пересечение с остальными region, выдает, естесственно false, поскольку region не с кем не 
            //пересекается, после этого происходит перемещение всех текстур уже в нужном направлении
            //и, конечно же, region-ы пересекаются и дальше ничего не двигается. Фууух.

            //можно будет еще поработать над проверкой, например, проверять ближайшие (это навряд ли,
            //поскольку вроде вычисление расстояние займет больший объем времени, нежели проверка
            //на пересечение) или ввести определенные области, из которых и будет в качестве фактического
            //параметра подставляться массив MoveTexture и их наследников.

            //все, теперь нужно найти запись/чтение из файла для создания первого уровня.
            //не говори гоп, пока не оттестируешь. Добавил в Game1 текстуру mt1, помимо
            //существовавшей ранее mt (теперь их две). Но теперь ни с одной из текстур не 
            //работают столкновения. Поступлю следующим образом: при создании mt, не стану добавлять ее в
            //класс AllStouns - сделал. Текстура mt1 на столкновения обрабатывается. Значит,
            //ошибка в классе или AllStouns или в процедуре IsCanGet
            //Подумав о том, что может быть, на всякий случай ввел домолнительные скобки - вдруг поможет.
            //конечно же, не помогло
            //Третий вариант поиска ошибки описан в классе AllStouns
            //        (!Way.Intersect(gr, AllStouns.textures[i].region)))
            //    { return true; }
            //}
            //return false;
            // - вот так выглядел код ранее. Нынешний его вид можно посмотреть сейчас
            for (i = 0; i < AllStouns.MaxTextures; i++)
                /*if((AllStouns.textures[i] != null) && 
                    (!(new Rectangle((int)(GetRectangle.X + speed.X + (speed.X > 0 ? 3 : -3)),
                        (int)(GetRectangle.Y + speed.Y + (speed.Y > 0 ? 3 : -3)),
                        (int)GetRectangle.Width,
                        (int)GetRectangle.Height).Intersects(AllStouns.textures[i].region))))*/
                /*if ((AllStouns.textures[i] != null &&
                    Way.Intersect(gr, AllStouns.textures[i].region)) ||
                    return true;
                if(AllStouns.stouns[i] != null &&
                    Way.Intersect(gr, AllStouns.stouns[i].region)))
                    return false;*/
                if ((AllStouns.textures[i] != null &&
                    gr.Intersects(AllStouns.textures[i].region)) ||
                    (AllStouns.stouns[i] != null &&
                    gr.Intersects(AllStouns.stouns[i].region)))
                    return false;
                
            return true;
        }

        public static bool IsCanGet2(Rectangle GetRectangle, Vector2 speed)
            //а вот эта процедура не будет включать в себя столкновения
            //с объектами MoveTexture и будет служить именно для 
            //объектов MoveTexutere
        {
            int i;
            Rectangle gr = new Rectangle((int)(GetRectangle.X - speed.X),
                (int)(GetRectangle.Y - speed.Y),
                GetRectangle.Width,
                GetRectangle.Height); 
            for (i = 0; i < AllStouns.MaxTextures; i++)
                if ((AllStouns.stouns[i] != null &&
                    gr.Intersects(AllStouns.stouns[i].region)))
                    return false;
            return true;
        }

        public static bool Intersect(Rectangle r1, Rectangle r2)
            //возвращает true, если два прямоугольника пересекаются
            //точки прямоугольника r2 поочередно проверяются на нахождение
            //в прямоугольнике r1
        {
            //во время тестирования выяснилось, что желательно
            //, чтобы rectangle пересекались даже в том случае, если 
            //пересекаются их грани - если углом зайти на угол, то дви-
            //жение будет невозможно в две стороны

            //Почему-то функция не восприняла большие rectangle, посему
            //буду использовать встроенную функцию
            if ((r2.X >= r1.X) &&
                (r2.X <= r1.X + r1.Width) &&
                (r2.Y >= r1.Y) &&
                (r2.Y <= r1.Y + r1.Height))
                return true;
            if ((r2.X + r2.Width >= r1.X) &&
                (r2.X + r2.Width <= r1.X + r1.Height) &&
                (r2.Y >= r1.Y) &&
                (r2.Y <= r1.Y + r1.Width))
                return true;
            if ((r2.X >= r1.X) &&
                (r2.X <= r1.X + r1.Width) &&
                (r2.Y + r2.Height >= r1.Y) &&
                (r2.Y + r2.Height <= r1.Y + r1.Height))
                return true;
            if ((r2.X + r2.Width >= r1.X) &&
                (r2.X + r2.Width <= r1.X + r1.Width) &&
                (r2.Y + r2.Height >= r1.Y) &&
                (r2.Y + r2.Height <= r1.Y + r1.Height))
                return true;
            return false;

        }

        public static bool Intersect(Rectangle r, Vector2 p)
        {
            if((r.Left < p.X) &&
                (r.Right > p.X) &&
                (r.Top < p.X) &&
                (r.Bottom > p.X))
                return true;
            return false;
        }
    }
}
