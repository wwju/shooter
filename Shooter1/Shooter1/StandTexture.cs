﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    //класс интерактивной неподвижной текстуры
    public class StandTexture:ForkTexture
    {
        //креатор
        public StandTexture(Texture2D texture, Vector2 position)
            : base(texture, position, false)
        { }

        public StandTexture(ContentManager Manager, Vector2 position, string textureName)
            : base(Manager, position, textureName, false)
        { }
    }
}
