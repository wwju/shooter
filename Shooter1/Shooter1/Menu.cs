﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    public class Menu
    {
        //кнопки
        TextButton newGame, options, exit;
        //шрифт
        SpriteFont spriteFont;
        //фон
        //ElTexture fone;
        

        public Menu(ContentManager Manager)
        {
            //локальные переменные. При создании
            //класса они будут загружаться в самом 
            //креаторе
            Texture2D buttonTexture, foneTexture;
            SpriteFont sf;

            //загрузка ресурсов
            sf = Manager.Load<SpriteFont>("MyFont");
            foneTexture = Manager.Load<Texture2D>("Tux");
            buttonTexture = Manager.Load<Texture2D>("ToMenu");

            //Создание кнопок и фона
            newGame = new TextButton(buttonTexture, new Vector2(40, 50), "New Game");
            options = new TextButton(buttonTexture, new Vector2(40, 100), "Options");
            exit = new TextButton(buttonTexture, new Vector2(40, 150), "Exit");
            //fone = new ElTexture(foneTexture, new Vector2(0, 0)); 

            //присвоение фона
            this.spriteFont = sf;
        }

        public void Update()
        {
            newGame.Update();
            options.Update();
            exit.Update();
            if (newGame.isDown)
                Show.isShow = 1;
            if (options.isDown)
                Show.isShow = 0;
            if (exit.isDown)
                Show.isShow = -1;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //fone.Draw(spriteBatch);
            /*newGame.Draw(spriteBatch, spriteFont);
            options.Draw(spriteBatch, spriteFont);
            exit.Draw(spriteBatch, spriteFont);*/
        }
    }
}
