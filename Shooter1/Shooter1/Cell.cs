﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    //Это зачаток клетки, которая будет использоваться
    //для передвижения по карте монстров с помощью алгоритма 
    //нахождения кратчайшего расстояния. Лучше всего сделать структурой
    //(если что переделаю, но сейчас это именно то что нужно. Объекты
    //на основе ее не будут вызываться из других классов. Нет будут, но
    //вроде члены структуры могут быть статическими. Что мне и нужно. :)
    public struct Cell
    {
        public Vector2 position; //Она будет отвечать именно за 
        //координату ячейки в реальном пространстве
        public Vector2 indexPosition; //Эта переменная будет
        //нести смысловую нагрузку, используемую для вычислений пути
        // - какой конкретно индекс по х и у ячпейка занимает на карте
        //нумерацию планирую вести с нуля, возможно, я ее закоменчу,
        //поскольку она по-любому будет дублировать данные массива
        public bool IsStoun;
        //в яндекс.словарях нашел impassable - непроходимый, но пока буду
        //использовать привычный "камень"
        public bool IsDetect; //указывает на то, определена ли переменная
        //будет отвечать за то, следует ли ее изменять или нет
        public int distance; //указывает на расстояние от точки начала
        //движения к объекту следования
        public Vector2 previousPosition; //указывает, какая позиция была 
        //предыдущей. Кстати, должна указывать не реальную пиксельную
        //позицию, а индекс.
        public bool IsChange;



        public Cell(Vector2 indexP, bool IsStoun)
        {
            indexPosition = indexP;
            position = indexPosition * 60; //Нужно будет ввести статическую переменную, 
            //отвечающую за размер 
            //текстуры, пока просто напишу 60
            this.IsStoun = IsStoun;
            IsDetect = false;
            distance = 30000;//ну например 30000. Можно задать любое большое число, чтобы
            //на сто процентов (почти) была вероятность того, что при сравнении это число не
            //окажется больше
        }

        public void Update(Vector2 vector)
        {
            this.position += vector;
        }
    }
}
