using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Общие сведения о сборке задаются следующим 
// набором атрибутов. Измените значения атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("Shooter1")]
[assembly: AssemblyProduct("Shooter1")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyCopyright("Copyright ©  2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Задание значения "false" для ComVisible скрывает типы из сборки от  
// компонентов COM.  При необходимости обращения к типу из сборки через 
// COM задайте для атрибута ComVisible этого типа значение "true". Только сборки Windows
// поддерживают COM.
[assembly: ComVisible(false)]

// В Windows следующий GUID является идентификатором библиотеки типов,
// если проект публикуется в COM. На других платформах он уникальным образом идентифицирует
// контейнер хранения игры при развертывании сборки на устройстве.
[assembly: Guid("54bca408-91a3-46a9-8839-962571339b05")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер построения
//      Редакция
//
[assembly: AssemblyVersion("1.0.0.0")]
