﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    public struct Particle //Класс частицы
    {
        //public Texture2D texture;// - временно (а может и навсегда)
        //лишил структуру текстуры
        public Vector2 position;
        Vector2 speed;
        public bool IsAlive; //Переменная отвечает за
        //"наличие жизни" у структуры. Тру если
        //частица жива, фолс - в противном. Это 
        //будет использоваться в управляющем классе
        //который будет в цикле проверять, "жива" ли
        //частица, и если она мертва, то устанавливать
        //на ее место новую (то есть управляющий класс)\
        //будет управлять массивом частиц
        //Vector2 Where; - эта переменная будет 
        //добавлена позже при добавлении ботов
        float TimeLife; //временно введена для тестирования


        //и не забыть добавить функцию, проверяющую
        //удары частицы со стенами и ботами

        public Particle(Vector2 BeginPosition) //конструктор частицы
            //здесь задается текстура и начальная позиция (позиция отправления частицы)
            //возможно, стоит текстуру удалить из структуры и назначить управление текстурой
            //управляющему классу - 
            //так и сделаю
        {
            //texture = Manager.Load<Texture2D>("spark");
            //эту строчку я оставлю на всякий случай
            //если понадобится использовать текстуру 
            //еще куда-либо (на словах не объясниить)
            //this.texture = texture;
            IsAlive = true;
            this.position = BeginPosition;
            speed = Way.Speed2(BeginPosition, Cursor.position, 3);
            TimeLife = 400;
        }

        public void Update(Vector2 vector)
        {
            position = position + speed + vector; // на этой строчке вроде бы закончилось
            //добавление неподвижных и движимых текстур  в класс MoveTextures. Конечно же, впоследствии
            //исходный код придется не раз изменять при смене архитектуры и т.п., но все же.
            //Ах, да /*speed*2 +*/ закоменчено для тестирования - если текстура при передвижении
            //героя не двигается, значит, все отлично :)
            TimeLife--;
            if (TimeLife < 0)
                IsAlive = false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(texture, hero_position, Color.White);
        }
    }
}
