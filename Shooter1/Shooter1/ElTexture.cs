﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    //это класс элементарной текстуры,
    //которая применяться будет для 
    //неинтерактивных неподвижных
    //графических объектов
    public class ElTexture:Position
    {
        //раздел описания переменных
        protected Texture2D texture; //временно переименовал переменную
        //в дальнейшем из нее следует сделать свойство
        //public Vector2 hero_position;
        //public bool IsInMap; - я закомментил эту переменную, потому
        //что она фактически не нужна. 

        //И еще можно сделать как в наследуемых классах: в креатор
        //в качестве формального параметра добавить ContentManager для загрузки 
        //текстур прямо в объекте

        public void LoadContent(ContentManager Manager, string textureName)
        {
            texture = Manager.Load<Texture2D>(textureName);
        }
        //креатор
        public ElTexture(Texture2D texture, Vector2 position, bool IsInMap = true)
            : base(position, IsInMap)
        {
            //this.IsInMap = IsInMap;
            this.texture = texture;
            this.position = position;
            /*if(IsInMap)
                MoveTextures.AddTexture(this);*/
        }

        public ElTexture(Vector2 position, bool IsInMap = true)
            :base(position, IsInMap)
        {
            //this.IsInMap = IsInMap;
            this.position = position;
            /*if (IsInMap)
                MoveTextures.AddTexture(this);*/
        }

        //рендеринг
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
