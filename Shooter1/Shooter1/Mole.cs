﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class Mole:Monstre
        //Но глядя на его эскиз, возникает желание
        //назвать класс "Кролик", возможно это будет 
        //даже более верным
    {
        public Mole(ContentManager Manager, Vector2 position, string textureName)
            : base(Manager, position, textureName, true)
        { }

        public void Update(Vector2 hero_position)
        {
            base.Update();
            r = (Way.Distance(this.position, hero_position));
            if ((r < 300d) && (r > 35d))
            {
                speed = Way.Speed2(position, hero_position, 3);
                if(Way.IsCanGet2(region, new Vector2(-speed.X,0)))
                    this.position.X += speed.X;
                if (Way.IsCanGet2(region, new Vector2(0, -speed.Y)))
                    this.position.Y += speed.Y;
            }
            if (r < 300d)
                scale = (float)Angle.GetAngleInRad(this.position, hero_position);
        }
    }
}
