﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class AllMoles
    {
        public static Mole[] moles;
        public static int Max, i;

        public AllMoles()
        {
            Max = 100;
            moles = new Mole[100];
        }

        public void AddMole(ContentManager Manager, Vector2 position)
        {
            for (i = 0; i < Max; i++)
                if (moles[i] == null)
                {
                    moles[i] = new Mole(Manager, position, "FlyMouse");
                    break;
                }
        }

        public void Update(Vector2 position)
        {
            for (i = 0; i < Max; i++)
                if (moles[i] != null)
                {
                    moles[i].Update(position);
                }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (i = 0; i < Max; i++)
                if (moles[i] != null)
                {
                    moles[i].Draw(spriteBatch);
                }
        }
    }
}
