﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class FlyMouses
    {
        public static FlyMouse[] flyMouses;
        public static int MaxMouses, i;

        public FlyMouses()//:base(new Vector2(0,0))
        {
            MaxMouses = 100;
            flyMouses = new FlyMouse[100];
        }

        public void AddFlyMouse(ContentManager Manager, Vector2 position)
        {
            for (i = 0; i < MaxMouses; i++)
                if (flyMouses[i] == null)
                {
                    flyMouses[i] = new FlyMouse(Manager, position, "FlyMouse");
                    break;
                }
        }

        public void Update(Vector2 position)
        {
            for (i = 0; i < MaxMouses; i++)
                if (flyMouses[i] != null)
                {
                    flyMouses[i].Update(position);
                }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (i = 0; i < MaxMouses; i++)
                if (flyMouses[i] != null)
                {
                    flyMouses[i].Draw(spriteBatch);
                }
        }
    }
}
