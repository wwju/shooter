﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    //эту текстуру я создал исключительно (хотя в дальнейшем посмотрим) для
    //разветвления древа текстур на StandTexture и MoveTexture. Фактически,
    //класс реализует сразу оба класса. И сделан он только для визуального 
    //различия классов, которые движутся по экрану (именно по экрану), и которые на 
    //экране неподвижны. Герой и кнопки - неподвижны, то есть не подвержены
    //действиям класса MoveTextures
    public class ForkTexture:ElTexture
    {
        //маска
        public Rectangle region;
        public bool IsStoun;


        protected void SetRegion(Vector2 position, float xScale, float yScale) //ах, да, это же нужно включить в само свойство - 
            //но этим займусь позже. Блин, нужно было закоментировать, для чего же мне все-таки эта процедура в смысле практического
            //назначения. А не удалить ли мне ее? Ан нет, у нее protected, и используется... нет, не используется. Вполне возможно,
            //я нечаяно скопировал эту функцию из другого проекта (хотя я и не помню, чтоб копировал что-либо. Ах, этот склероз! :))
            //и еще невнимательность. Используется в креаторах. Нужно еще в Update добавить. Хотя нет, не нужно пока. Дело в том,
            //что эта процедура вычисляет еще и масштабирование. Мне здесь это не нужно. Точно скопировал!
        {
            this.region = new Rectangle((int)position.X, (int)position.Y,
                    (int)(texture.Width * xScale), (int)(texture.Height * yScale));
        }


        //креатор
        public ForkTexture(Texture2D texture, Vector2 position,  /*loat xScale = 1, float yScale = 1,*/ bool IsInMap)
            : base(texture, position, IsInMap)
        {
            region = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            //SetRegion(hero_position, xScale, yScale);
            //this.IsStoun = IsStoun;
        }


        //пока что (читай навсегда - но я могу еще поменять свое нескромное мнение) я закомментил переменные масштаба. 
        //Если понадобятся - раскомментю. Но вряд ли они будут применяться
        public ForkTexture(ContentManager Manager, Vector2 position, string textureName,/* float xScale = 1, float yScale = 1,*/ bool IsInMap)
            : base(position, IsInMap)
        {
            base.LoadContent(Manager, textureName);
            region = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            //SetRegion(hero_position, xScale, yScale);
            //this.IsStoun = IsStoun;
        }


        public virtual void Update()
        {
            region = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
        }


        //просто и понятно - рендеринг наследуется
        //из родительского класса и остается без
        //изменений в этом классе
        /*public new void  Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, hero_position, Color.White);
        }*/
    }
}
