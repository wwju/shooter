﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Shooter1
{
    public static class Angle //Статический класс, используемый для
        //вычисления углов в градусах и радианах на основе позиций
        //курсора и фактического параметра координаты текстуры
        //Возможно, стоит добавить измерение угла между двумя фактическими
        //координатами. Да, спустя неделю или две эта функция понадобилась
        //для вычисления пути монстров
    {
        
        public static double GetAngleInGrad(Vector2 position) //Функция вычисляет
            //значение угла между курсором и текстурой в градусах. Возможно, стоит 
            //переименовать
        {
            double result; //Эта переменная возвратит значение, но сперва она будет 
            //использоваться для вычислений

            //В некоторых случаях вместо обычных чисел будут использоваться численные
            //константы, чтобы не загромождать память вычислениями известных величин
            //и исходный код повторяющимися участками
            if (Cursor.position.Y == position.Y)
            {
                if (Cursor.position.X == position.X)
                    return 0;
                else
                    if (Cursor.position.X > position.X)
                        return 1.570796326794897;
                    else
                        return 4.71238898038469;
            }
            else
            {
                result = Math.Atan((Cursor.position.X - position.X) / (position.Y - Cursor.position.Y)) * 180 / Math.PI;
                if (Cursor.position.Y > position.Y)
                    result = 180 + result;
                else
                    result = 360 + result;
            }
            return result;
        }

        public static double GetAngleInGrad(Vector2 position, Vector2 position2) //Функция вычисляет
        //значение угла между курсором и текстурой в градусах. Возможно, стоит 
        //переименовать
        {
            double result; //Эта переменная возвратит значение, но сперва она будет 
            //использоваться для вычислений

            //В некоторых случаях вместо обычных чисел будут использоваться численные
            //константы, чтобы не загромождать память вычислениями известных величин
            //и исходный код повторяющимися участками

            //я наугад заменил Cursor.hero_position на position2, возможно стоило 
            //сделать почти наоборот
            if (position2.Y == position.Y)
            {
                if (position2.X == position.X)
                    return 0;
                else
                    if (position2.X > position.X)
                        return 1.570796326794897;
                    else
                        return 4.71238898038469;
            }
            else
            {
                result = Math.Atan((position2.X - position.X) / (position.Y - position2.Y)) * 180 / Math.PI;
                if (position2.Y > position.Y)
                    result = 180 + result;
                else
                    result = 360 + result;
            }
            return result;
        }

        public static double GetAngleInRad(Vector2 position) //Функция преобразует угол в градусах из 
            //функции GetAngleInGrad в угол в радианах
        {
            double result;
            result = GetAngleInGrad(position);
            result = result * Math.PI / 180; //Эта формула преобразует угол в градусах в угол в радианах
            return result;
        }

        public static double GetAngleInRad(Vector2 position, Vector2 position2) //Функция преобразует угол в градусах из 
        //функции GetAngleInGrad в угол в радианах
        {
            double result;
            result = GetAngleInGrad(position, position2);
            result = result * Math.PI / 180; //Эта формула преобразует угол в градусах в угол в радианах
            return result;
        }
    }
}
