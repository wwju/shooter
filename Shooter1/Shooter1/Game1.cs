using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    /// <summary>
    /// Это главный тип игры
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Cursor cursor;
        Hero hero;
        SpriteFont sf;
        //Menu menu;
        //SkillPanel skillPanel;
        Particles p;
        //MoveTexture mt;
        //MoveTexture mt1;
        Map map;
        //FlyMouse fm;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Позволяет игре выполнить инициализацию, необходимую перед запуском.
        /// Здесь можно запросить нужные службы и загрузить неграфический
        /// контент.  Вызов base.Initialize приведет к перебору всех компонентов и
        /// их инициализации.
        /// </summary>
        protected override void Initialize()
        {
            // ЗАДАЧА: добавьте здесь логику инициализации

            base.Initialize();
        }

        /// <summary>
        /// LoadContent будет вызываться в игре один раз; здесь загружается
        /// весь контент.
        /// </summary>
        protected override void LoadContent()
        {
            // Создайте новый SpriteBatch, который можно использовать для отрисовки текстур.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            cursor = new Cursor(Content);
            hero = new Hero(Content);
            //menu = new Menu(Content);
            //skillPanel = new SkillPanel();
            //skillPanel.LoadContent(Content);
            p = new Particles(Content);
            //mt = new MoveTexture(Content, new Vector2(100, 100), "Grass");
            //mt1 = new MoveTexture(Content, new Vector2(600, 600), "Grass");
            map = new Map(Content);
            sf = Content.Load<SpriteFont>("MyFont");
            //fm = new FlyMouse();
            
            // ЗАДАЧА: используйте здесь this.Content для загрузки контента игры
        }

        /// <summary>
        /// UnloadContent будет вызываться в игре один раз; здесь выгружается
        /// весь контент.
        /// </summary>
        protected override void UnloadContent()
        {
            // ЗАДАЧА: выгрузите здесь весь контент, не относящийся к ContentManager
        }

        /// <summary>
        /// Позволяет игре запускать логику обновления мира,
        /// проверки столкновений, получения ввода и воспроизведения звуков.
        /// </summary>
        /// <param name="gameTime">Предоставляет моментальный снимок значений времени.</param>
        protected override void Update(GameTime gameTime)
        {
            // Позволяет выйти из игры
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                //this.Exit();
            if (Show.isShow == -1)
            {
                //отсюда игра выходит нафиг
                this.Exit();
            }
            //обновляется меню
            if (Show.isShow == 0)
            {
                //добавить сюда код обновления меню
                //menu.Update();
            }

            if (Show.isShow == 1)
            {
                //добавить сюда код обновления первого лэвла

            }
            //курсор будет выводиться независимо от остальных обстоятельств
            cursor.Update();
            //mt.Update();
            //mt1.Update();
            map.Update(hero.position);
            hero.Update(p);
            //if (Cursor.isDown)
                //this.Exit();
            p.Update();
            

            // ЗАДАЧА: добавьте здесь логику обновления

            base.Update(gameTime);
        }

        /// <summary>
        /// Вызывается, когда игра отрисовывается.
        /// </summary>
        /// <param name="gameTime">Предоставляет моментальный снимок значений времени.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Brown);
            spriteBatch.Begin();
            //рендерится меню
            if (Show.isShow == 0)
            {
                //добавить сюда код рендера меню
                
            }

            if (Show.isShow == 1)
            {
                //добавить сюда код обновления первого лэвла
                //skillPanel.Draw(spriteBatch);
            }
            map.Draw(spriteBatch);
            //menu.Draw(spriteBatch);
            cursor.Draw(spriteBatch);
            hero.Draw(spriteBatch);
            p.Draw(spriteBatch);
            //spriteBatch.DrawString(sf, Convert.ToString(FlyMouses.count), new Vector2(0, 0), Color.White);
            //mt.Draw(spriteBatch);
            //mt1.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
