﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class Monstre:MoveTexture
    {
        //по-хорошему, здесь необходимо создать основную информацию,
        //индивидуальную для каждого монстра всех видов, а логику
        //перенести в управляющий класс. 

        public bool IsAlive;
        public Vector2 speed;
        protected float scale;
        protected SpriteFont spriteFont;
        protected double r;
        //int index;
        //public int hp;

        public Monstre(ContentManager Manager, Vector2 position, string textureName, bool IsStoun)
            : base(Manager, position, textureName, IsStoun)
        {
            IsAlive = true;
            scale = 0;
            spriteFont = Manager.Load<SpriteFont>("MyFont");
            //index = Monsters.AddTexture(this);
        }

        public override void Update()
        {
            base.Update();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture,//текстура, 
                    new Vector2(position.X + region.Width / 2, position.Y + region.Height / 2),
                    new Rectangle(0,0, texture.Width, texture.Height),//прямоугольник опредляющий в пикселях исходные текстру,
                    Color.White,//цвет,
                    scale,//поворот, 
                    new Vector2(region.Width / 2, region.Height / 2),
                    1, //масштаб
                    SpriteEffects.None,//эффекты спрайта,
                    1);//глубина
            spriteBatch.DrawString(spriteFont, Convert.ToString(r), position, Color.Black);
        }
    }
}
