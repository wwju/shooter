﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class TextButton:ButtonTexture
    {
        String caption;
        Vector2 textPosition;

        public TextButton(Texture2D texture, Vector2 position, String caption)
            : base(texture, position)
        {
            this.caption = caption;
            textPosition = new Vector2(3, 3) + position;
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            base.Draw(spriteBatch);
            spriteBatch.DrawString(font,//шрифт, 
                caption,//строка, 
                textPosition,//позиция, 
                Color.Black);//цвет)
        }
    }
}
