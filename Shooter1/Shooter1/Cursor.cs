﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    public class Cursor:ElTexture
    {
        //свойство, изменить таким образом, чтобы оно
        //контролировало позицию из родительского класса
        public static new Vector2 position { get; private set; }
        //указывает, нажата или левая кнопка мыши
        public static bool isDown;

        public Cursor(ContentManager Manager)
            :base(new Vector2(40,0), false)
        {

            base.LoadContent(Manager, "cursor");
            isDown = false;
        }

        public void Update()
        {
            MouseState ms;
            ms = Mouse.GetState();
            position = new Vector2(ms.X, ms.Y);
            if (ms.LeftButton == ButtonState.Pressed)
                isDown = true;
            else
                isDown = false;
        }

        public new void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
