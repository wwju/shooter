﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    public class Hero:StandTexture
    {
        //раздел описания переменных
        float speed; //скорость
        float scale; //угол наклона, "лицо"
        //должно следовать за курсором
        int time_to_fire;
        StandTexture st;
        

        //креатор
        public Hero(ContentManager Manager):base(Manager,new Vector2(300,300), "Grass")
        {
            //IsInMap = false;
            speed = 5;
            scale = 0;
            time_to_fire = 0;
            st = new StandTexture(Manager, new Vector2(100, 100), "Grass");
        }

        public void Fire()
        {
            
        }

        /*protected void NormalAngle(ref double angle)
        {
            while (angle>359.99) angle=angle-360;
            while (angle<0) angle=360+angle;
        }

        protected double GetAngle()
        {
            double result;
            if(Cursor.hero_position.Y == hero_position.Y)
            {
                if (Cursor.hero_position.X == hero_position.X)
                   return 0;
               else
                    if (Cursor.hero_position.X > hero_position.X)
                       return 1.570796326794897; 
                   else
                       return 4.71238898038469;
            }
            else
            {
                result = Math.Atan((Cursor.hero_position.X - hero_position.X) / (hero_position.Y - Cursor.hero_position.Y)) * 180 / Math.PI;
                if (Cursor.hero_position.Y > hero_position.Y)
                    result = 180 + result;
                else
                    result = 360 + result;
            }
            //NormalAngle(ref result);
            result = result * Math.PI / 180;
            return result;
        }*/

        //обновление
        public void Update(Particles particles)
        {
            //base.region = new Rectangle((int)hero_position.X, (int)hero_position.Y, 60, 60);
            //управление с клавы
            base.Update();
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                //hero_position.X -= speed;
                if (Way.IsCanGet(region, new Vector2(speed, 0)))
                    MoveTextures.ChangePosition(new Vector2(speed, 0));
                /*else
                    MoveTextures.ChangePosition(new Vector2(-speed - 3, 0));*/
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                if (Way.IsCanGet(region, new Vector2(0, -speed)))
                    MoveTextures.ChangePosition(new Vector2(0, -speed));
                /*else
                    MoveTextures.ChangePosition(new Vector2(0, speed + 3));*/
            }
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                if (Way.IsCanGet(region, new Vector2(0, speed)))
                    MoveTextures.ChangePosition(new Vector2(0, speed));
                /*else
                    MoveTextures.ChangePosition(new Vector2(0, -speed - 3));*/
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                if (Way.IsCanGet(region, new Vector2(-speed, 0)))
                    MoveTextures.ChangePosition(new Vector2(-speed, 0));
                /*else
                    MoveTextures.ChangePosition(new Vector2(speed + 3, 0));*/
            }

            //написал передвижение через стены с зажатым шифт для тестирования
            //класса Map, когда паресер неправильно воспринимал xml
            if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                {
                        MoveTextures.ChangePosition(new Vector2(speed, 0));
                }
                if (Keyboard.GetState().IsKeyDown(Keys.S))
                {
                        MoveTextures.ChangePosition(new Vector2(0, -speed));
                }
                if (Keyboard.GetState().IsKeyDown(Keys.W))
                {
                    MoveTextures.ChangePosition(new Vector2(0, speed));
                }
                if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                        MoveTextures.ChangePosition(new Vector2(-speed, 0));
                }
            }
            scale = (float)Angle.GetAngleInRad(position);
            

            /*MouseState ms;
            ms = Mouse.GetState();*/
            //это проба алгоритма нахождения кратчайшего пути
            time_to_fire --;
            
            if ((Cursor.isDown)&&(time_to_fire < 1))
            {
                //hero_position = Way.NextPoint(hero_position, Angle.GetAngleInGrad(hero_position));
                //hero_position += Way.Speed(hero_position,  Angle.GetAngleInRad(hero_position));
                if (time_to_fire <= 0)
                {
                    particles.AddParticle(this.position);
                    time_to_fire = 30;
                }
            }

            
            //сюда поместить код, управляющий углом 
            //наклона текстуры по формуле, предполагаемо
            // с2 = а2 + в2 +2ав*кос(альфа),
            //но возможны и другие
            //кос(альфа) = (с2 - а2 - в2)/2ав
            /*if ((Cursor.hero_position.X > hero_position.X) &&
                (Cursor.hero_position.Y < hero_position.Y))
                //scale = Math.Asin((double)((Cursor.hero_position.X - hero_position.X) / (Cursor.hero_position.Y - hero_position.Y)));
                scale = (float)Math.Asin((double)(((Cursor.hero_position.Y - hero_position.Y) / (Cursor.hero_position.X - hero_position.X))) * Math.PI / 180);
            //else scale = 0;
            if ((Cursor.hero_position.X < hero_position.X) &&
                (Cursor.hero_position.Y < hero_position.Y))
                scale = (float)((180 * Math.PI / 180 - Math.Asin((double)(((Cursor.hero_position.Y - hero_position.Y) / (Cursor.hero_position.X - hero_position.X))) * Math.PI / 180)) + 90 * Math.PI / 180);
            if ((Cursor.hero_position.X < hero_position.X) &&
                (Cursor.hero_position.Y > hero_position.Y))
                scale = (float)((270 * Math.PI / 180 - Math.Asin((double)(((Cursor.hero_position.Y - hero_position.Y) / (Cursor.hero_position.X - hero_position.X))) * Math.PI / 180)) + 180 * Math.PI / 180);
            if ((Cursor.hero_position.X > hero_position.X) &&
                (Cursor.hero_position.Y > hero_position.Y))
                scale = (float)((360 * Math.PI / 180 - Math.Asin((double)(((Cursor.hero_position.Y - hero_position.Y) / (Cursor.hero_position.X - hero_position.X))) * Math.PI / 180)) + 270 * Math.PI / 180);
            //scale++;
            */
            /*
            Для нахождения угла по его синусу, косинусу и т.д.используются 
             * так называемые аркфункции: арксинус, арккосинус и т.д. 
             * Их обозначают arcsin a, arccos a и т.д.
            На Вашем калькуляторе над кнопками с синусом и косинусом есть надписи: 
             * sin в степени -1 и cos в степени -1.Это создатели калькулятора так кратко обозначили аркфункции. 
             * Чтобы ими воспользоваться, надо набрать число( например, 0,4965), нажать клавишу SHIFT или 2nd, 
             * а затем клавишу, над которой написано cos в степени -1 и равно. 
             * У Вас получится угол, косинус которого равен 0,4965.*/
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //условие в дальнейшем необходимо убрать
            //Чтобы его убрать, необходимо исправить
            //закомментированный код выше, который
            //будет поворачивать текстру на определенный угол
            //if((scale >= 1)||(scale <= -1))
                spriteBatch.Draw(texture,//текстура, 
                    //hero_position,//позиция, 
                    new Vector2(position.X + region.Width/2, position.Y + region.Height/2),//это позиция - я ее
                    //изменил для того, чтобы он нормально отображался в координатной плоскости. Что было раньше,
                    //можно увидеть, раскомментив строчку выше.
                    //region,//прямоугольник опредляющий в пикселях исходные текстру,
                    new Rectangle(0, 0, texture.Width, texture.Height), //ну вот и найдена
                    //очень давняя ошибка. Как оказалось, этот прямоугольник указывает, 
                    //какой прямоугольник из данной текстуры отобразить, а не тот
                    //прямоугольник, который показывается выше.
                    Color.White,//цвет,
                    scale,//поворот, 
                    new Vector2(region.Width / 2, region.Height / 2),
                    1, //масштаб
                    SpriteEffects.None,//эффекты спрайта,
                    0);//глубина
                //st.Draw(spriteBatch);
            /*else
                spriteBatch.Draw(texture,//текстура, 
                    hero_position,//позиция, 
                    region,//прямоугольник опредляющий в пикселях исходные текстру,
                    Color.White,//цвет,
                    scale,//поворот, 
                    new Vector2(region.Width / 2, region.Height / 2),
                    1, //масштаб
                    SpriteEffects.None,//эффекты спрайта,
                    0);//глубина*/

            //процедура дописана, удалять комменты не буду - на память
        }
    }
}
