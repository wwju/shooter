﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    public class Particles : ElTexture //класс будет добавлять частицы в массив частиц
        //и следить за их жизнью, рендерить, обновлять состояние
    {
        Particle[] particles;
        int i, g;
        int MaxParticles;
        //позиция будет в данном случае обозначать смещение
        //координат частиц относительно координат
        //Texture2D texture; //текстура, используемая для 
        //отображения частицы

        public Particles(ContentManager Manager):base(new Vector2(0,0), true)
        {
            MaxParticles = 100;
            particles = new Particle[MaxParticles];
            texture = Manager.Load<Texture2D>("spark");
        }

        public void AddParticle(Vector2 position)
        {
            for (i = 0; i < MaxParticles; i++)
                if (!particles[i].IsAlive)
                {
                    particles[i] = new Particle(position);
                    break;
                }
        }

        public void Update()
        {
            for (i = 0; i < MaxParticles; i++)
                if (particles[i].IsAlive)
                    particles[i].Update(position);
            position = new Vector2(0, 0);
            //здесь понадобилась функция, определяющая, пересекается ли
            //точка с прямоугольником. Буду писать ее в Way
            for (i = 0; i < MaxParticles; i++)
                if (particles[i].IsAlive)
                {
                    for (g = 0; g < AllStouns.MaxTextures; g++)
                        if((AllStouns.stouns[g] != null) &&
                            AllStouns.stouns[g].region.Intersects(new Rectangle((int)particles[i].position.X, (int)particles[i].position.Y, 8,8)))
                            particles[i].IsAlive = false;
                    for (g = 0; g < FlyMouses.MaxMouses; g++)
                        if ((FlyMouses.flyMouses[g] != null) &&
                            FlyMouses.flyMouses[g].region.Intersects(new Rectangle((int)particles[i].position.X, (int)particles[i].position.Y, 8, 8)))
                        {
                            FlyMouses.flyMouses[g] = null;
                            particles[i].IsAlive = false;
                        }
                    for (g = 0; g < AllMoles.Max; g++)
                        if ((AllMoles.moles[g] != null) &&
                            AllMoles.moles[g].region.Intersects(new Rectangle((int)particles[i].position.X, (int)particles[i].position.Y, 8, 8)))
                        {
                            //ох, ступил я, здесь нужно было просто 
                            AllStouns.textures[AllMoles.moles[g].indexInStouns] = null;
                            AllMoles.moles[g] = null;
                            particles[i].IsAlive = false;
                            
                        }
                }
        }

        override public void Draw(SpriteBatch spriteBatch)
        {
            for (i = 0; i < MaxParticles; i++)
                if (particles[i].IsAlive)
                    spriteBatch.Draw(texture, particles[i].position + position, Color.White);
        }
    }
}
