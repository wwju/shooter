﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    //класс текстуры, которая бегает по экрану! (ЭКРАНУ!)
    //или за его пределами, и следовательно, подвержено
    //действию класса MoveTextures
    class MoveTexture:ForkTexture
    {
        public int indexInStouns;
                //креатор
        public MoveTexture(Texture2D texture, Vector2 position)
            : base(texture, position, true)
        {
            indexInStouns = AllStouns.AddTexture(this); //а вот над этим нужно оочень подумать - по-моему на основе
            //MoveTexture нужно создать класс StounMoveTexture и [прозрачный]MoveTexture
        }

        public MoveTexture(ContentManager Manager, Vector2 position, string textureName, bool IsStoun = true)
            : base(Manager, position, textureName, true)
        {
            if(IsStoun)
                indexInStouns = AllStouns.AddTexture(this);
        }


    }
}
