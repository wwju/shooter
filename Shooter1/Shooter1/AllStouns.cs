﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    static class AllStouns
    {
        public static MoveTexture[] textures;
        public static Stoun[] stouns;
        public static int MaxTextures;
        static int i;
        //static int index;

        //по поводу обнаруженной мною ранее ошибки, которую я описал в классе Way
        // - при количестве текстур более одной столкновения вообще не обрабатываются.
        //При проверке решил пойти старым способом:
        /*textures[index] = texture;
            index++;*/
        //при этом выяснилось, что ошибка скорее всего таится не в этом классе - 
        //несмотря на этот код, все равно проверка на столкновения не работала

        //А процедуры добавления текстур нужно сделать функциями, которые будут сообщать
        //текстурам их идентафикационные номера, а затем можно будет убирать с помощью
        //процедуры DeleteTexture ненужные текстуры

        public static int AddTexture(MoveTexture texture)
        {
            //вот чувствую я, что этот класс мне может быть не нужен,
            //ну разве что для считывания информации о карте. Пошел делать
            //выстрелы в нужном направлении. Поэтому я и закомментировал 
            //то что было скопировано из класса MoveTextures. Но, на всякий случай,
            //оставляю... пока не придет в голову гениальнейшая идея
            //или нет, пожалуй как минимум для тестирования оно мне будет нужно
            for (i = 0; i < MaxTextures; i++)
                if (textures[i] == null)
                {
                    textures[i] = texture;
                    return i;
                }/**/
            //пока что я меняю код на код с исопльзованием конечного индекса, то есть я не смогу
            //сейчас удалять объекты - но мне и не надо

            /*textures[index] = texture;
            index++;*/
            //пока что работает, нужно будет исправить код ButtonTexture или код меню - надписи не двигаются за кнопками
            return -1;
        }

        public static void AddTexture(Stoun stoun)
        {
            for (i = 0; i < MaxTextures; i++)
                if (stouns[i] == null)
                {
                    stouns[i] = stoun;
                    break;
                }
        }

        static AllStouns()
        {
            MaxTextures = 1000;
            textures = new MoveTexture[MaxTextures];
            stouns = new Stoun[MaxTextures];
            //index = 0;
        }

        public static void DeleteStoun(int index)
        {
            stouns[i] = null;
        }
    }
}
