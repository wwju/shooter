﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class ButtonTexture:StandTexture
    {
        protected bool inCursor;
        public bool isDown { get; private set; }

        public ButtonTexture(Texture2D texture, Vector2 position)
            : base(texture, position)
        {
            isDown = false;
        }

        public override void Update()
        {
            base.Update();
            if ((Cursor.position.X > position.X) &&
                (Cursor.position.X < position.X + region.Width) &&
                (Cursor.position.Y > position.Y) &&
                (Cursor.position.Y < position.Y + region.Height))
                inCursor = true;
            else
                inCursor = false;
            if (inCursor && Cursor.isDown)
                isDown = true;
            else
                isDown = false;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (inCursor)
                spriteBatch.Draw(texture, position, Color.Black);
            else
                base.Draw(spriteBatch);
        }
    }
}
