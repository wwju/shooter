﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Shooter1
{
    class FindingTracts:Position
        //перевел в яндекс.словарях "нахождение, пути",
        //вроде верно. Класс будет служить, КЭП, для 
        //нахождения кратчайшего пути. Но, судя по всему,
        //он из этого пути должен будет выдавать лишь следующую
        //точку следования монстра. Будет загружать информацию о
        //непроходимых областях в свою память, на основе этого
        //сообщать объектам структуры Cell, нужно ли его использовать
        //для рассчетов или нет. 
        //Предполагается сделать так: унаследовать класс от класса 
        //Position, далее обновлять положения клеток как в классе 
        //Particles, и обязательно делать перерасчет непроходимых областей.
        //Вот насчет перерасчета нужно подумать: если сделать перерасчет до 
        //начала движения текстур, то 
        //если делать перерасчет после начала движения, то информация не будет
        //соответствовать реальной. Все отлично!, сперва нужно поместить пересчет 
        //непроходимых областей, затем 

        //Нужно не забыть о том, что расстояние между точками по-диагонали и по-вертикали 
        //различно, потому расстояние считать нужно по алгоритму А* (наверное, я еще
        //не читал его :) Класс должен быть нестатическим, наследоваться от
        //класса Position, и во время выполнения процедуры Update должен
        //обновлять позиции объектов Cell.

        //Запускать двумерный цикл и у всех cells, у которых дистанция равна 30000 (или
        //переменная, отвечающая за ранюю активацию равна false) искать кратчайший путь предыдущего
        //графа и вносить его значение в поле previousPosition. А вот по поводу этого 
        //нужно подумать. Дело в том, что клетка не сможет изменить свое значение
        //после того, как заполнены все восемь клеток вокруг нее, и она пересчитана

        //Ах, и можно ввести перед тем как считать кратчайший путь, процедуру, которая определит,
        //сможет ли объект дойти до конца, не задев препятствия. Если может - то кратчайший путь вычислять ненужно,
        //и достаточно продолжать движение объекта в нужном направлении

        //можно ввести переменную, отвечающую за необходимость изменять значение
        //сперва все переменные, кроме начальной точки установлены в необходимость изменения
        //запускается двумерный цикл. Все переменные, которые можно изменять через switch
        //ищут значение ближайшей клетки, если таковая есть. Нужно ввести еще переменную,
        //которая будет говорить, изменялось ли значение. Тогда не нужно будет сравнивать
        //значение с селлами, которые сами не изменялись. Во время каждого цикла каждая
        //клетка, значение которой было или не было измененено, проверяется на окруженность
        //измененными клетками. Если она полностью окружена, то не имеет смысла в следующих
        //итерациях ее измерять
    {

        Cell[,] cells;
        int i, g;
        int width, height;


        public void Update()
        {
            for (i = 0; i < this.width; i++)
                for (g = 0; g < this.width; g++)
                    cells[i, g].Update(position);
            position = new Vector2(0, 0);
        }

        public FindingTracts(float width, //пиксельная длина карты
            float height                 //пиксельная высота карты  
            ):base(new Vector2(0,0))
        {
            this.width = (int)(width / 60);
            this.height = (int)(height / 60);
            int f; //дополнительная переменная цикла
            cells = new Cell[this.width, this.height]; //инициализирование массива селлс
            //в этом массиве будет происходить заполнение селлс данными
            for(i = 0; i < this.width; i++)
                for (g = 0; g < this.width; g++)
                {
                    bool b = true; //переменная 
                    Rectangle gr = new Rectangle(i * 60, g * 60, 60, 60);
                    for (f = 0; f < AllStouns.MaxTextures; f++)
                        if ((AllStouns.stouns[f] != null &&
                            gr.Intersects(AllStouns.stouns[f].region)))
                            b = false;
                    cells[i,g] = new Cell(new Vector2(i, g), b);
                }
        }

        public void Find(Vector2 beginPosition,
            Vector2 endPosition)
        {
            //код, который преобразует реальное положение в индекс
            Vector2 beginIndexPosition, endIndexPosition; //а вот это индексы
            beginIndexPosition = beginPosition / 60;
            endIndexPosition = endPosition / 60; //я думаю, это верно, но я написал
            //наобум и не проверял, а стоит.

            //код, который задает начальные значения всем селлам
            for (i = 0; i < this.width; i++)
                for (g = 0; g < this.width; g++)
                {
                    cells[i, g].IsDetect = false;
                    cells[i, g].IsChange = false;
                }
            cells[(int)beginIndexPosition.X,(int)beginIndexPosition.Y].IsDetect = true;
            cells[(int)beginIndexPosition.X,(int)beginIndexPosition.Y].IsChange = true;//пока на начальном
            //этапе, ночью, ничего не вижу, поэтому вписал все что мог


            //код, который вычисляет путь. Код будет основан на while
            //пока(селл[конечная].МожноИзменять)
            //и сюда уже вписывать двумерный цикл.

            //а вот здесь объявлены переменные, которые будут использоваться
            //для хранения значений во время подсчета, и затем после подсчета
            //реальным значениям присваивать эти значения. Имена этих переменных
            //соответствуют именам переменных класса Cell
            Vector2 previousPosition; 
            float distance;
            while(cells[(int)endIndexPosition.X,(int)endIndexPosition.Y].IsDetect)//кстати, сюда нужно 
                //добавить условие, если дистанция перевалила за овер90000, то подсчет не иначе как следует
                //прекратить, дабы избежать вечного двигателя, ой цикла. А вот вечный двигатель отнюдь не по
                //помешает. Да, ночью я пишу бреэээддд Гыц лол нЯ! Да еще и под диксов колбасюсь, а еще
                //как идиот пишу это в комменты, надеясь, что их никто не увидит
                for (i = 0; i < this.width; i++)
                    for (g = 0; g < this.width; g++)
                    {
                        //если(ячейка.МожноИзменять)
                        //сравнивается со всеми
                        //присваивается нужные значение, если нужно и изменяется исчэйндж


                    }
            //а после пересчитываются значения исдетект

            //А отсюда просто возвратить нужное значение. Какое оно будет,
            //я не знаю, так что предсказать не могу.
        }
    }
}
