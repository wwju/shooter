﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    //А вот тут, прежде чем вписать хоть одну переменную и функцию,
    //нужно хорошо все продумать и обсудить.

    //Предполагаю делать так: сделать класс статическим,
    //сообщаться с классом Show. При переходе на другую карту
    //класс Show создает новый объект Map. Но в этом случае
    //класс не сможет быть статическим... Значит, нужно будет
    //ввести процедуру ChangeMap, которая будет считывать информацию
    //из файлов и инициализировать переменные, тем самым эта 
    //процедура будет фактически являться
    //креатором. В файле будет содержаться информация о 
    //непроходимых областях, которая будет применяться
    //в функции Way.IsCanGet. Также должна содержаться 
    //просто текстура карты (в формате png, jpeg и т.п.)
    //По поводу хранения мест рождения монстров и героев
    //(если я решу возрождать героев и рождать монстров
    //, а не сделаю каждый шаг ведущим к смерти, а 
    //монстры будут рождаться только при создании карты
    //Хотя, с другой стороны, так вполне должно быть - 
    //при создании карты обязаны родится монстры, а иначе бегать
    //по карте и ждать их появления будет ну очень не интересно.
    //Также можно при создании карты рандомно генерировать 
    //монстров, а после (по желанию) постепенно рождать их 
    //снова и снова. 
    //И еще можно ввести "смертельные" объекты - ямы, ядовитые цветы,
    //бочки со взрывчаткой, при соприкосновении с которыми герой умирает.
    //А вообще нужно подумать над унаследованием классом Map класса
    //ElTextures или MoveTextures. Но для этого класс ElTextures нужно
    //унаследовать от еще одного класса, содержащего всего одно свойство:
    //hero_position - это сделал (От класса Position теперь наследуется
    //класс ElTexture). Вроде удачно. И кстати, почему я не унаследовал
    //класс сразу от класса ElTexture. Дело в том, что управление
    //позициями текстуры карты и непроходимых областей я буду 
    //осуществлять через костыль, подобно классу Particles - каждый вызов
    //процедуры Update класса я буду прибавлять к координатам всех текстур
    //и rectangle не позицию, а вектор, который буду обнулять после 
    //всех инкрементов. Таким образом, hero_position будет служить мне
    //в качестве вектора направления. Блин... А вот тут-то я ступил: класс-то
    //ведь статический, и следовательно, он не сможет обращаться к переменной
    //hero_position из класса Position, блин! А ведь еще он не будет иметь доступ 
    //вообще ни к каким элементам базового класса Position. Выход один - сделать
    //класс не статическим. Но теперь нужно будет как-то передавать 

    //Хм. Новая идея - этот класс будет просто считывать в статические классы
    //информацию из файлов. А также будет просто управлять одной текстурой карты.
    //Вопрос стоит в том, где она будет создаваться. По-идее, она должна создаваться
    //в классе Game1. Там и создастся. А вот со сменой карты как быть? Нужно будет 
    //в Update класса Game1 организовать проверку, и, если что, производить смену
    //карты

    //Версию для проверки я сделал - 
    //MoveTexture mt;
    //    public Map(ContentManager Manager):base(new Vector2(0,0))
    //    {
    //        mt = new MoveTexture(Manager, new Vector2(-300, -300), "Grass");
    //        //AllStouns.AddTexture(mt);
    //        //MoveTextures.AddTexture(mt); - это я ступил, ведь в креаторе уже 
    //        //прописан этот код
    //    }
    //    public void Update()
    //    {
    //        hero_position = new Vector2(0, 0);
    //        mt.Update();
    //    }
    //    public override void Draw(SpriteBatch spriteBatch)
    //    {
    //        mt.Draw(spriteBatch);
    //    }
    //и она работает. Я очень этому рад лол
    //но возникла проблема: 

    public class Map:Position
    {
        //провожу тестирование класса Stoun, смысл которого заключается
        //в том, чтобы на карте были непроходимые места. Сперва он не хотел
        //вообще запускаться (КЭП, так всегда бывает), потом запустился,
        //но вместо вводимых ректанглов 10*1000 он дает мне только 
        //1*1, что меня очень не устраивает :( Оказалось, проблема в 
        //моей собственной процедуре, которая проверяет пересечение.
        //Родная функция прекрасно справилась со своим заданием

        //MoveTexture mt;
        //Stoun st;
        XmlDocument doc;
        Stoun[] stouns;
        int i;
        Texture2D fon;
        FlyMouses flyMouses;
        AllMoles moles;


        public Map(ContentManager Manager):base(new Vector2(0,0))
        {
            //заработало! - теперь на карту добавляются непроходимые области
            //вот нельзя говорить гоп, пока не перепрыгнешь. А сказал.
            //Теперь мучаюсь, почему при считывании из XML файла парсер
            //последние значения ширины и высоты приравнивает к более ранним.
            //для решения этой проблемы я первым делом строчку
            //Rectangle r = new Rectangle(0, 0, 0, 0);
            //перенес после начала первого цикла, чтобы оно каждый раз 
            //приравнивалось к нулю.
            //после этого я узнал, что эти самые высота и ширина становились
            //равны тому, что было до них. А если до них был 0, то они и становились
            //нулями. После этого я внимательно просмотрел код xml и вспомнил, что
            //я его писал в двух файлах. Ну так вот, в одном файле я изменил
            //ветки <height> на <h> и с шириной соответственно (сократил)
            //а после скопировал длинные к коротким. Потому вышла такая неразбериха.
            //Хотя, дописал в Hero.Update процедуру, при которой при зажатии Shift
            //герой свободно проходит сквозь стены
            fon = Manager.Load<Texture2D>("Map");
            stouns = new Stoun[100];
            //mt = new MoveTexture(Manager, new Vector2(-300, -300), "Grass");
            //st = new Stoun(new Rectangle(10, 10, 1, 1000), Manager);
            i = 0;
            //Rectangle r = new Rectangle(0, 0, 0, 0);
            //Этот код парсит *.xml-файл, в котором находятся координаты и размеры
            //непроходимых областей
            //Благодарю хабраюзера shortcaster, за опубликованный им пост, 
            //который научил меня работать с xml :)
            doc = new XmlDocument();
            doc.LoadXml(System.IO.File.ReadAllText("doc.xml"));
            XmlNodeList items = doc.GetElementsByTagName("Stoun");
            foreach (XmlNode p in items)
            {
                Rectangle r = new Rectangle(0, 0, 0, 0);
                foreach (XmlNode xc in p.ChildNodes)
                {
                    switch (xc.Name)
                    {
                        case "x":
                            r.X = Convert.ToInt32(xc.InnerText);
                            break;
                        case "y":
                            r.Y = Convert.ToInt32(xc.InnerText);
                            break;
                        case "w":
                            r.Width = Convert.ToInt32(xc.InnerText);
                            break;
                        case "h":
                            r.Height = Convert.ToInt32(xc.InnerText);
                            break;
                        //хм... почему-то не совсем так, как я хочу работает - 
                        //парсер невоспринимает вроде так, как нужно.
                        //или что-то еще. Уже голова кружится :(
                        //почему-то, начиная с четвертого
                    }    
                }
                stouns[i] = new Stoun(r, Manager, i);
                i++;
            }

            flyMouses = new FlyMouses();
            //flyMouses.AddMole(Manager, new Vector2(400, 400));
            Vector2 fm_position;
            items = doc.GetElementsByTagName("FlyMouse");
            foreach (XmlNode p in items)
            {
                fm_position = new Vector2(0, 0);
                foreach (XmlNode xc in p.ChildNodes)
                {
                    switch (xc.Name)
                    {
                            //но по-хорошему после того, как я добавлю передвижение 
                            //монстров по клеткам, можно будет в xml-таблицу добавить
                            //и третий пункт, который будет указывать на то, которы
                        case "x":
                            fm_position.X = (float)Convert.ToDouble(xc.InnerText);
                            break;
                        case "y":
                            fm_position.Y = (float)Convert.ToDouble(xc.InnerText);
                            break;
                    }
                }
                flyMouses.AddFlyMouse(Manager, fm_position);
            }
            //AllStouns.AddTexture(mt);
            //MoveTextures.AddTexture(mt); - это я ступил, ведь в креаторе уже 
            //прописан этот код
            //stouns[34] = new Stoun(new Rectangle(400, 400, 100, 100), Manager);
            ////stouns[35] = new Stoun(new Rectangle(1000, 400, 400, 400), Manager);                    /*for (i = 0; i < 100; i++)
            //            if (stouns[i] == null)
            //            {
            //                stouns[i] = new Stoun(r, Manager, i);
            //                break;
            //            }*/

            moles = new AllMoles();
            moles.AddMole(Manager, new Vector2(400, 400));
        }

        public void Update(Vector2 heroPosition)
        {
            //hero_position = new Vector2(0, 0);
            //mt.Update();
            //st.Update();
            for (i = 0; i < 100; i++)
                if (stouns[i] != null)
                    stouns[i].Update();
            flyMouses.Update(heroPosition);
            moles.Update(heroPosition);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //mt.Draw(spriteBatch);
            //st.Draw(spriteBatch);
            spriteBatch.Draw(fon, position, Color.White);
            for (i = 0; i < 100; i++)
                if (stouns[i] != null)
                    stouns[i].Draw(spriteBatch);
            flyMouses.Draw(spriteBatch);
            moles.Draw(spriteBatch);
        }
    }
}
