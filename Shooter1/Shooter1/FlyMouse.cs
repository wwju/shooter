﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Shooter1
{
    class FlyMouse:Monstre
    {
        public FlyMouse(ContentManager Manager, Vector2 position, string textureName)
            : base(Manager, position, textureName, false)
        { }

        public void Update(Vector2 hero_position)
        {
            base.Update();
            r = (Way.Distance(this.position, hero_position));
            if ((r < 300d) && (r > 60d))
            {
                speed = Way.Speed2(position, hero_position, 3);
                this.position += speed;
            }
            if (r < 300d)
                scale = (float)Angle.GetAngleInRad(this.position, hero_position);
        }
    }
}
